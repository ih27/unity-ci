﻿
using UnityEditor;

public static class BuildScript
{
    static void BuildAndroid()
    { 
        string[] defaultScene = { "Assets/Scenes/MainScene.unity" };
        BuildPipeline.BuildPlayer(defaultScene, "./builds/game.apk",
            BuildTarget.Android, BuildOptions.None);
    }
}
